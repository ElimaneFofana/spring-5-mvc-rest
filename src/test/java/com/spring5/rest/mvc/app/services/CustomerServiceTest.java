package com.spring5.rest.mvc.app.services;

import com.spring5.rest.mvc.app.api.mapper.CustomerMapper;
import com.spring5.rest.mvc.app.api.model.CategoryDTO;
import com.spring5.rest.mvc.app.api.model.CustomerDTO;
import com.spring5.rest.mvc.app.domain.Category;
import com.spring5.rest.mvc.app.domain.Customer;
import com.spring5.rest.mvc.app.repositories.CustomerRepository;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

/**
 * Created by Elimane on May, 2018, at 05:36
 */
public class CustomerServiceTest {

    public static final Long ID_1 = 1L;
    public static final Long ID_2 = 2L;

    public static final String FIRSTNAME = "Jim";
    public static final String LASTNAME = "Rohn";

    public static final String FIRSTNAME_1 = "Anthony";
    public static final String LASTNAME_1 = "Robbins";

    @Mock
    CustomerRepository customerRepository;

    CustomerService customerService;


    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        customerService = new CustomerServiceImpl(CustomerMapper.INSTANCE,customerRepository);
    }

    @Test
    public void getAllCustomers() throws Exception {

        //given
        List<Customer> categories = Arrays.asList(new Customer(), new Customer(), new Customer());

        when(customerRepository.findAll()).thenReturn(categories);

        //when
        List<CustomerDTO> customerDTOS = customerService.getAllCustomers();

        //then
        assertEquals(3, customerDTOS.size());
    }

    @Test
    public void getCustomerByFirstName() throws Exception {
        //given
       Customer customer = new Customer();
        customer.setId(ID_1);
        customer.setFirstname(FIRSTNAME_1);

//        Customer customer1 = new Customer();
//        customer1.setId(ID_2);
//        customer1.setFirstname(FIRSTNAME);

        when(customerRepository.findById(anyLong())).thenReturn(java.util.Optional.ofNullable(customer));

        //when
        CustomerDTO customerDTO = customerService.getCustomerById(ID_1);

        //then
        assertEquals(ID_1, customerDTO.getId());
        assertEquals(FIRSTNAME_1, customerDTO.getFirstname());
    }

    @Test
    public void saveCustomerByDTO() throws Exception {

        //given
        CustomerDTO customerDTO = new CustomerDTO();
        customerDTO.setFirstname("Jim");

        Customer savedCustomer = new Customer();
        savedCustomer.setFirstname(customerDTO.getFirstname());
        savedCustomer.setLastname(customerDTO.getLastname());
        savedCustomer.setId(1l);

        when(customerRepository.save(any(Customer.class))).thenReturn(savedCustomer);

        //when
        CustomerDTO savedDto = customerService.saveCustomerByDTO(1L, customerDTO);

        //then
        assertEquals(customerDTO.getFirstname(), savedDto.getFirstname());
        assertEquals("/api/v1/customer/1", savedDto.getCustomerUrl());
    }

}