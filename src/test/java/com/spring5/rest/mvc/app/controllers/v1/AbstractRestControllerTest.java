package com.spring5.rest.mvc.app.controllers.v1;

import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Created by Elimane on May, 2018, at 14:56
 */
public class AbstractRestControllerTest {

    public static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

}
