package com.spring5.rest.mvc.app.repositories;

import com.spring5.rest.mvc.app.domain.Customer;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by Elimane on May, 2018, at 03:27
 */
public interface CustomerRepository extends JpaRepository<Customer,Long>{
    //public Customer findById(Long id);
}
