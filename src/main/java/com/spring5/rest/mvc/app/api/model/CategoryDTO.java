package com.spring5.rest.mvc.app.api.model;

import lombok.Data;

/**
 * Created by Elimane on May, 2018, at 02:28
 */
@Data
public class CategoryDTO {

    private Long id;
    private String name;

}
