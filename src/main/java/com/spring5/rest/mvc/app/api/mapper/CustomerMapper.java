package com.spring5.rest.mvc.app.api.mapper;

import com.spring5.rest.mvc.app.api.model.CategoryDTO;
import com.spring5.rest.mvc.app.api.model.CustomerDTO;
import com.spring5.rest.mvc.app.domain.Category;
import com.spring5.rest.mvc.app.domain.Customer;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

/**
 * Created by Elimane on May, 2018, at 03:21
 */
@Mapper
public interface CustomerMapper {

    CustomerMapper INSTANCE = Mappers.getMapper(CustomerMapper.class);

    @Mapping(source = "id", target = "id")
    CustomerDTO customerToCustomerDTO(Customer customer);

    @Mapping(source = "id", target = "id")
    Customer customerDtoToCustomer(CustomerDTO customerDTO);

}
