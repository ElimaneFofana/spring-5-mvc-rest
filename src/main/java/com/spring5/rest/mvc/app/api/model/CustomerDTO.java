package com.spring5.rest.mvc.app.api.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by Elimane on May, 2018, at 03:18
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CustomerDTO {

    private Long id;
    private String firstname;
    private String lastname;
    @JsonProperty("customer_url")
    private String customerUrl;
}
