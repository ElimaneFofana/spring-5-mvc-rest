package com.spring5.rest.mvc.app.api.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * Created by Elimane on May, 2018, at 03:18
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CustomerListDTO {
List<CustomerDTO> customers;
}
