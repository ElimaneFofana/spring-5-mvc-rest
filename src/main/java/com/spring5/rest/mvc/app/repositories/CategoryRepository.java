package com.spring5.rest.mvc.app.repositories;

import com.spring5.rest.mvc.app.domain.Category;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by Elimane on May, 2018, at 02:30
 */
public interface CategoryRepository extends JpaRepository<Category, Long>{
   public Category findByName(String name);
}
