package com.spring5.rest.mvc.app.api.mapper;

import com.spring5.rest.mvc.app.api.model.CategoryDTO;
import com.spring5.rest.mvc.app.domain.Category;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

/**
 * Created by Elimane on May, 2018, at 03:27
 */
@Mapper
public interface CategoryMapper {

    CategoryMapper INSTANCE = Mappers.getMapper(CategoryMapper.class);

    @Mapping(source = "id",target = "id")
    CategoryDTO categoryToCategoryDTO(Category category);
}
