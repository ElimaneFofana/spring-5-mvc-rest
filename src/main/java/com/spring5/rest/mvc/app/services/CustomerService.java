package com.spring5.rest.mvc.app.services;

import com.spring5.rest.mvc.app.api.model.CategoryDTO;
import com.spring5.rest.mvc.app.api.model.CustomerDTO;

import java.util.List;

/**
 * Created by Elimane on May, 2018, at 03:24
 */
public interface CustomerService {

    public List<CustomerDTO> getAllCustomers();

    public CustomerDTO getCustomerById(Long id);

    public  CustomerDTO createNewCustomer(CustomerDTO customerDTO);

    public CustomerDTO saveCustomerByDTO(Long id, CustomerDTO customerDTO);

    CustomerDTO patchCustomer(Long id, CustomerDTO customerDTO);

}
