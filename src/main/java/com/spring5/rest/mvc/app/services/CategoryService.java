package com.spring5.rest.mvc.app.services;

import com.spring5.rest.mvc.app.api.model.CategoryDTO;
import com.spring5.rest.mvc.app.domain.Category;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by Elimane on May, 2018, at 02:33
 */
public interface CategoryService {

    public List<CategoryDTO> getAllCategories();

    public CategoryDTO getCategoryByName(String name);

}
